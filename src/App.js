import Header from './components/Header';
import Basket from './components/Basket';
import { useState, useEffect } from 'react';
import ProductList from './components/ProductList';
import { apiClient } from "./http-common";
function App() {
  const [cartHeader, setCartHeader] = useState({});
  const [cartContents, setCartContents] = useState([]);
  const [cartLoader, setcartLoader] = useState('loading');

  const updateCart = async () => {
    setcartLoader("loading");
    var res = await apiClient.get("api/cart", {
      headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257" }
    });
    setCartHeader([cartHeader, res.data.data.cartHeader]);
    setCartContents([cartContents, res.data.data.cartItem]);
    setcartLoader("finish");
  }

  const onAdd = async (product) => {
    setcartLoader("loading");
    await apiClient.post("api/cart", { productId: product.id, qty: 1 }, {
      headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257" }
    });
    updateCart();
  };

  const onRemove = async (id) => {
    let text = "Are you sure to delete ?";
    if (window.confirm(text) === true) {
      setcartLoader("loading");
      await apiClient.delete("api/cart", {
        headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257" },
        data: {
          productId: id
        }
      });
      updateCart();
    }
  };

  const onMinus = async (id,qty) => {
    setcartLoader("loading");qty--;
    await apiClient.put("api/cart", {productId: id,qty } ,{
      headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257" }
    });
    updateCart();
  };

  const onPlus = async (id,qty) => {
    setcartLoader("loading");qty++;
    await apiClient.put("api/cart",{productId: id,qty } ,{
      headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257" }
    });
    updateCart();
  };

  useEffect(() => {
    updateCart();
    document.title = "Dzahin Shop"
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="App">
      <Header></Header>
      <div className="container">
        <div className="row">
          <div className="col-8 p-3 mt-3 bg-light border rounded-3">
            <ProductList onAdd={onAdd}></ProductList>
          </div>
          <div className="col-4 mt-3 bg-light border rounded-3" style={{ position: "relative" }}>
            <Basket
              cartHeader={cartHeader}
              cartContents={cartContents}
              cartLoader={cartLoader}
              onRemove={onRemove}
              onMinus={onMinus}
              onPlus={onPlus}
            ></Basket>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;