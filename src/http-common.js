import axios from "axios";
var apiClient = axios.create({
  baseURL: "https://shop.dzahin.com",
  headers: {
    "Content-type": "application/json",
    "x-api-key":"12345678"
  }
});

var reqPlain = axios.create({
  baseURL: "https://shop.dzahin.com",
  headers: {
    "Content-type": "application/json"
  }
});

export {apiClient,reqPlain}