import React from 'react';

export default function Header(props) {
  return (

    <header className="p-3 bg-dark text-white">
      <div className="container">
        <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <b>Dzahin Shop</b>
          <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
            <li></li>
          </ul>
          <div className="text-end">
            <button type="button" className="btn btn-outline-light me-2">Agus d Prayogo</button>
          </div>
        </div>
      </div>
    </header>
  );
}