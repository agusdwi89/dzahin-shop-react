import React from 'react';
import {apiClient,reqPlain} from "./http-common";
import Product from './components/Product';
import Pagination from './components/Pagination';

export default class ProductList extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      keyword : "",
      url:"/api/product?page=0&size=9",
      products: [],
      info : {
        isFirst : false,
        isLast : false,
        totalData : 0,
        page :0,
        nextPage : 1,
        prevPage : 0
      }
    }
  }

  prevClick(p,k,e) {
    e.getProduct(p,k);
  }
  nextClick(p,k,e){
    e.getProduct(p,k);
  }

  getProduct(p,key=""){
    var url = `/api/product?page=`+p+`&size=9`;
    if(key!==""){
      url = `/api/product/search?key=`+key+`&page=`+p+`&size=9`;
    }

    apiClient.get(url)
    .then(res => {
      const products = res.data.data.content;
      const pagin = res.data.data; 
      this.setState({ products });
      const info = {
        isFirst : pagin.first,
        isLast : pagin.last,
        totalData : pagin.totalElements,
        offset : pagin.pageable.offset+pagin.pageable.pageSize,
        page :pagin.pageable.pageNumber,
        nextPage : pagin.pageable.pageNumber+1,
        prevPage : (pagin.pageable.pageNumber == 0)?0:pagin.pageable.pageNumber-1,
        nextClick : this.nextClick,
        prevClick : this.prevClick,
        key:key
      }
      this.setState({info});
    })

  }

  onSearch(){
    this.getProduct(0,this.state.keyword);
  }

  onClear(){
    this.setState({keyword:""})
    this.getProduct(0);
  }

  handleChange(event) {
    this.setState({keyword: event.target.value});
  }

  componentDidMount() {
    {   
    // var res = await reqPlain.get("/");
    
    // console.log(res);
    
    // res = await apiClient.post("api/token",{email: "agusdwi89@gmail.com"})
    // res = await apiClient.post("api/cart", {productId:55,qty:1}, {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // });
    // res = await apiClient.post("api/cart", {productId:5,qty:1}, {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // });
    // res = await apiClient.put("api/cart", {productId:55,qty:10}, {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // });
    // res = await apiClient.delete("api/cart", {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"},
    //   data: {productId:55}
    // });
    // res = await apiClient.get("api/cart",{
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // })
    // var idc = res.data.data.cartItem[0].items[0].id;
    // console.log("number  --> "+idc)

    // res = await apiClient.post("api/cart/checkout", {cartId:[idc]}, {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // });

    // res = await apiClient.post("api/cart/checkout/order", {cartId:[idc]}, {
    //   headers: { "token": "ddc6e6ad-c7e2-47e5-8187-3b5b1984e257"}
    // });

    // console.log("number  --> "+idc)
    }

    this.getProduct(0);
  }

  render() {
    // let a = "";
    // console.log(a);
    // this.state.products.map((product) => (console.log(product)));

    const { products } = this.state;
    return (
     <>
        <div className='row mb-3'>
          <h5 className="col-sm" >Product List</h5>
          <div className="col-sm input-group search-top">
            <div className="form-outline">
              <input onChange={(e)=>this.handleChange(e)} value={this.state.keyword} type="search" className="form-control" placeholder='search product'/>
            </div>
            <button onClick={() => this.onClear()} type="button" className="btn btn-danger" >
              x
            </button>
            <button onClick={() => this.onSearch()} type="button" className="btn btn-primary" >
              search
            </button>
          </div>
        <div/> 
        </div>
          <div className='row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3'>
            {this.state.products.map((product) => (
              <Product key={product.id} product={product} onAdd={this.props.onAdd} ></Product>
            ))}
          </div>
          
        <Pagination parent={this} info={this.state.info} ></Pagination>
      </>
    )
  }
}


