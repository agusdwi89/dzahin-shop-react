import React from 'react';
import {apiClient} from "../http-common";
import Product from './Product';
import Pagination from './Pagination';
import loader from '../public/loader.gif';

export default class ProductList extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      productLoader : "finish",
      keyword : "",
      url:"/api/product?page=0&size=9",
      products: [],
      info : {
        isFirst : false,
        isLast : false,
        totalData : 0,
        page :0,
        nextPage : 1,
        prevPage : 0
      }
    }
  }

  prevClick(p,k,e) {
    e.getProduct(p,k);
  }
  nextClick(p,k,e){
    e.getProduct(p,k);
  }

  getProduct(p,key=""){
    this.setState({productLoader:"loading"})
    var url = `/api/product?page=`+p+`&size=9`;
    if(key!==""){
      url = `/api/product/search?key=`+key+`&page=`+p+`&size=9`;
    }

    apiClient.get(url)
    .then(res => {
      const products = res.data.data.content;
      const pagin = res.data.data; 
      this.setState({ products });
      const info = {
        isFirst : pagin.first,
        isLast : pagin.last,
        totalData : pagin.totalElements,
        offset : pagin.pageable.offset+pagin.pageable.pageSize,
        page :pagin.pageable.pageNumber,
        nextPage : pagin.pageable.pageNumber+1,
        prevPage : (pagin.pageable.pageNumber === 0)?0:pagin.pageable.pageNumber-1,
        nextClick : this.nextClick,
        prevClick : this.prevClick,
        key:key
      }
      this.setState({info});
      this.setState({productLoader:"finish"})
    })

  }

  onSearch(){
    this.getProduct(0,this.state.keyword);
  }

  onClear(){
    this.setState({keyword:""})
    this.getProduct(0);
  }

  handleChange(event) {
    this.setState({keyword: event.target.value});
  }

  componentDidMount() {   
    this.getProduct(0);
  }

  render() {
    return (
     <>
        <div className='row mb-3'>
          <h5 className="col-sm" >Product List</h5>
          <div className="col-sm input-group search-top">
            <div className="form-outline">
              <input onChange={(e)=>this.handleChange(e)} value={this.state.keyword} type="search" className="form-control" placeholder='search product'/>
            </div>
            <button onClick={() => this.onClear()} type="button" className="btn btn-danger" >
              x
            </button>
            <button onClick={() => this.onSearch()} type="button" className="btn btn-primary" >
              search
            </button>
          </div>
        <div/> 
        </div>
          <div className='row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 parent-loader-product'>

          {
            (this.state.productLoader === "loading")?
              <div className="loader-basket">
                <div className="loader-image">
                  <img src={loader} alt="loader" />
                </div>
              </div>:<></>
          }

            {this.state.products.map((product) => (
              <Product key={product.id} product={product} onAdd={this.props.onAdd} ></Product>
            ))}
          </div>
          
        <Pagination parent={this} info={this.state.info} ></Pagination>
      </>
    )
  }
}


