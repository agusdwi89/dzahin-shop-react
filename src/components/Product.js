import React from 'react';
import {numberWithCommas} from './Helper';

export default function Product(props) {
  const { product, onAdd } = props;
  return (
    <div className="col title-product">
      <div className='card shadow-sm'>
        
        {product.promo.length > 0 ?
          <div className="badge bg-dark text-white position-absolute">{product.promo}</div>
        :
          ""
        }
        <img alt="ok" className="card-img-top" src={"https://picsum.photos/id/"+product.id+"/450/200"}/>
        <div className="card-body p-4 pt-1">
          <div className="text-center">
            <small><i>{product.merchant.name}</i></small>
            <h5 className="fw-bolder">{product.name}</h5>
            Rp. {numberWithCommas(product.price)}
          </div>
        </div>
        <div className="text-center">
          <b></b>
          < button onClick={() => onAdd(product)} className="btn btn-outline-dark mt-auto mb-2">Add to cart</ button>
        </div>
      </div>
    </div>
  );
}