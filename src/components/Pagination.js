import React from 'react';

export default class Pagination extends React.Component {

  render() {
    return (
        <div className="row p-3 mt-3">
            <div className="col-sm">
                showing {this.props.info.offset} of {this.props.info.totalData} total data 
            </div>
            <div className="col-sm text-right">
                <nav aria-label="Page navigation example">
                    <ul className="pagination justify-content-end">
                        {this.props.info.isFirst? 
                            <li className='page-item disabled'>
                                <button  className="page-link" tabIndex="-1">Previous</button>
                            </li>
                        : 
                            <li className='page-item'>
                                <button onClick={() => this.props.info.prevClick(this.props.info.prevPage,this.props.info.key,this.props.parent)} 
                                    className="page-link" tabIndex="-1">Previous</button>
                            </li>
                        }
                        {this.props.info.isLast? 
                            <li className='page-item disabled'>
                                <button  className="page-link" tabIndex="-1">Next</button>
                            </li>
                        : 
                            <li className="page-item">
                                <button onClick={() => this.props.info.nextClick(this.props.info.nextPage,this.props.info.key,this.props.parent)} 
                                className="page-link">Next</button>
                            </li>
                        }
                    </ul>
                  </nav>
            </div> 
        </div>
    )
  }
}