import React from 'react';
import { numberWithCommas } from './Helper';
import loader from './../public/loader.gif';

export default function Basket(props) {
  const { onRemove, cartHeader, cartContents, cartLoader,onPlus,onMinus } = props;
  const items = (typeof cartContents[1] === 'undefined') ? [] : cartContents[1];
  const itemHeader = (typeof cartHeader[1] === 'undefined') ? [] : cartHeader[1];
  return (
    <>
      <h5 className="mt-3 mb-3">Cart Items</h5>
      <span className="badge badge-success bdg-total"> {itemHeader.totalItem} </span>
      <hr />
      <div className="parent-cart">

        {
          (cartLoader === "loading") ?
            <div className="loader-basket">
              <div className="loader-image">
                <img src={loader} alt="loader" />
              </div>
            </div> : <></>
        }

        <div>
          {items.map((item) => (
            <div key={item.merchant.id}>
              <b className="cart-merchant-title" key={item.merchant.id}>{item.merchant.name}</b>

              <ul className='cart'>
                {item.items.map((it) => (
                  <li key={it.id} className="row">
                    <div className="col-5">
                      <span className="cart-item-name">{it.product.name}</span>
                      <br />
                      <small><b>Price : {numberWithCommas(it.product.price)}</b></small>
                      <br/>
                      <small>
                        {
                          (it.product.promo.length > 0) ? <span>promo : {it.product.promo}</span> : <></>
                        }
                      </small>
                      <small><i>
                        {
                          (it.priceCalculate.freeQty > 0) ? <p>Bayar {it.priceCalculate.actualQty} gratis {it.priceCalculate.freeQty}</p> : <></>
                        }
                      </i></small>
                    </div>
                    <div className="col-3 cart-qty">

                      

                      <div className="btn-group" role="group" aria-label="Basic example">
                        <button onClick={() => onMinus(it.product.id,it.qty)} type="button" className="btn btn-secondary">-</button>
                        <button type="button" className="btn btn-secondary disabled"> {it.qty}</button>
                        <button onClick={() => onPlus(it.product.id,it.qty)} type="button" className="btn btn-secondary">+</button>
                      </div>

                    </div>
                    <div className="col-4 text-right price-cart">
                      <button onClick={() => onRemove(it.product.id)} type="button" className="del-cart btn btn-danger">x</button>
                      {(it.priceCalculate.defaultPrice !== it.priceCalculate.actualPrice) ? <><span className="price-cart"><del>{numberWithCommas(it.priceCalculate.defaultPrice)}</del></span><span className="price-cart">{numberWithCommas(it.priceCalculate.actualPrice)}</span></> : <span className="price-cart">{numberWithCommas(it.priceCalculate.defaultPrice)}</span>}
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
        <div>

          {itemHeader.totalItem !== 0 && (
            <>
              <hr></hr>
              <div className="row">
                <div className="col-7">Total Price</div>
                <div className="col-5 text-right">Rp. {numberWithCommas(itemHeader.defaultPrice)}</div>
              </div>
              <div className="row">
                <div className="col-7">Discount</div>
                <div className="col-5 text-right">Rp. {numberWithCommas(itemHeader.discountedPrice)}</div>
              </div>

              <div className="row">
                <div className="col-7">
                  <strong>Total Price</strong>
                </div>
                <div className="col-5 text-right">
                  <strong>Rp. {numberWithCommas(itemHeader.actualPrice)}</strong>
                </div>
              </div>
              <hr />
              <div className="row parent-btn-checkout">
                <button className="btn btn-primary btn-checkout" onClick={() => alert('Implement Checkout!')}>
                  Checkout
                </button>
              </div>
            </>
          )}
          {itemHeader.totalItem === 0 && (
            <div className="text-center"><br/> <br/> <b>cart empty</b></div>
          )}
        </div>
      </div>
    </>
  );
}